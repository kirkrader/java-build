/*
Copyright Kirk Rader 2018

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
*/

/*!
\page build-base-image base.dockerfile and build-base-image

\brief Dockerfile and convenience script for _registry.gitlab.com/kirkrader/javabuild:latest_

\section sec-build-base-image-prerequisites Prerequisites

- [Git](https://git-scm.com/)
- [Docker](https://www.docker.com/products/docker-engine)
- [bash](https://www.gnu.org/software/bash/)<sup>\ref note-windows "1"</sup>
- [sed](https://www.gnu.org/software/sed/)

\anchor note-windows
<sup>1</sup> Microsoft Windows<sub>&trade;</sub> users may use the version of
_bash_ installed by default by Git.

\section sec-build-base-image-files Files

- <https://gitlab.com/kirkrader/java-build/blob/master/docker/base.dockerfile>
- <https://gitlab.com/kirkrader/java-build/blob/master/build-base-image>

\section sec-build-base-image-usage Usage

\verbatim
git clone https://gitlab.com/kirkrader/java-build.git
cd java-build
git checkout -b yourbranch
find . \( \
  -name \*.dockerfile \
  -o -name \* \
  -o -name .gitlab-ci.yml \
  \) \
  -exec sed -i'.bak' 's+registry.gitlab.com/kirkrader+yourtagprefix+g' \{\} \;
./build-base-image
docker push yourtagprefix/java-build:latest
\endverbatim

[build-base-image](https://gitlab.com/kirkrader/java-build/blob/master/build-base-image)
takes no parameters. It is simply a convenience wrapper for building and tagging
an image using
[docker/base.dockerfile](https://gitlab.com/kirkrader/java-build/blob/master/docker/base.dockerfile).

*/
