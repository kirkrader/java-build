/*
Copyright Kirk Rader 2018

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
*/

/*!
\page build-gitlabci-image gitlabci.dockerfile and build-gitlabci-image

\brief Convenience script for gitlabci.dockerfile.

\section sec-build-gitlabci-image-prerequisites Prerequisites

- \ref build-base-image

\section sec-build-gitlabci-image-files Files

- <https://gitlab.com/kirkrader/java-build/blob/master/docker/gitlabci.dockerfile>
- <https://gitlab.com/kirkrader/java-build/blob/master/build-gitlabci-image>

\section sec-build-gitlabci-image-usage Usage

\verbatim
git clone https://gitlab.com/kirkrader/java-build.git
cd java-build
git checkout -b yourbranch
find . \( \
  -name \*.dockerfile \
  -o -name \* \
  -o -name .gitlab-ci.yml \
  \) \
  -exec sed -i'.bak' 's+registry.gitlab.com/kirkrader+yourtagprefix+g' \{\} \;
./build-base-image
./build-gitlabci-image
docker push yourtagprefix/gitlabci-java-build:latest
\endverbatim

[build-gitlabci-image](https://gitlab.com/kirkrader/java-build/blob/master/build-gitlabci-image)
takes no parameters. It is simply a convenience wrapper for building and tagging
an image using
[gitlabci.dockerfile](https://gitlab.com/kirkrader/java-build/blob/master/docker/gitlabci.dockerfile).

Such images are based on \ref build-base-image "base.dockerfile" but with
configuration suitable for use as a `image` referenced from a GitLab CI YAML
file. See this project's own CI script,
[.gitlab-ci.yml](https://gitlab.com/kirkrader/java-build/blob/master/.gitlab-ci.yml):

\include .gitlab-ci.yml

*/
