FROM registry.gitlab.com/kirkrader/java-build/centos:7

LABEL maintainer="Kirk Rader <apps@rader.us>"

LABEL  description="Dockerfile for a development environment image \
common to a team of users and their shared CI/CD build server"

LABEL copyright="Copyright 2018 Kirk Rader"

LABEL license="Licensed under the Apache License, Version 2.0 (the \
&quot;License&quot;); you may not use this file except in compliance with the \
License. You may obtain a copy of the License at \
http://www.apache.org/licenses/LICENSE-2.0 \ Unless required by applicable law \
or agreed to in writing, software distributed under the License is distributed \
on an &quot;AS IS&quot; BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, \
either express or implied. See the License for the specific language governing \
permissions and limitations under the License."

# Note: this Dockerfile should contain only the base development environment
# configuration. Put any user-specific configuration into user.dockerfile

# Copy dependency binary dependency and configuration files
COPY texlive.profile texlive.profile
COPY install-tl2018-unx.tar.gz install-tl2018-unx.tar.gz
COPY git-2.19.1.tar.gz git-2.19.1.tar.gz
COPY git-lfs-linux-amd64-v2.5.2.tar.gz git-lfs-linux-amd64-v2.5.2.tar.gz
COPY doxygen-1.8.15.tgz doxygen-1.8.15.tgz
COPY global-maven-settings.xml global-maven-settings.xml
COPY maven-3.5.4-bin.tar.gz maven-3.5.4-bin.tar.gz
COPY docker-compose-Linux-x86_64-1.22.0 docker-compose-Linux-x86_64-1.22.0
COPY plantuml-1.2018.11.jar /var/opt/plantuml/plantuml.jar
COPY batik-all-1.7.jar /var/opt/plantuml
COPY jlatexmath-minimal-1.0.3.jar /var/opt/plantuml
COPY jlm_cyrillic.jar /var/opt/plantuml
COPY jlm_greek.jar /var/opt/plantuml

# bring yum repository and installed packages up to date
RUN yum -y update && yum -y upgrade && \
  yum -y install epel-release-7-11

# install base development packages and dependencies
RUN yum -y install \
  asciidoc-8.6.8-5.el7 \
  autoconf-2.69-11.el7 \
  automake-1.13.4-3.el7 \
  bison-3.0.4-1.el7 \
  cmake-2.8.12.2-2.el7 \
  curl-devel-7.29.0-46.el7 \
  device-mapper-persistent-data-0.7.3-3.el7 \
  docbook2X-0.8.8-17.el7 \
  expat-devel-2.1.0-10.el7_3 \
  flex-2.5.37-3.el7 \
  gcc-c++-4.8.5-28.el7_5.1 \
  ghostscript-9.07-28.el7_4.2 \
  gettext-devel-0.19.8.1-2.el7 \
  glib-1.2.10-41.el7 \
  graphviz-2.30.1-21.el7 \
  java-1.8.0-openjdk-devel-1.8.0.181-3.b13.el7_5 \
  libtool-2.4.2-22.el7_3 \
  lvm2-2.02.177-4.el7 \
  make-3.82-23.el7 \
  openssl-devel:1.0.2k-12.el7 \
  perl-devel-5.16.3-292.el7 \
  perl-Digest-MD5-2.52-3.el7 \
  sudo-1.8.19p2-14.el7_5 \
  which-2.20-7.el7 \
  xmlto-0.0.25-7.el7 \
  yum-utils-1.1.31-46.el7_5 \
  zlib-devel-1.2.7-17.el7

# install docker CLI
RUN /usr/sbin/groupadd docker && \
  yum-config-manager \
    --add-repo https://download.docker.com/linux/centos/docker-ce.repo && \
  yum -y install docker-ce-18.06.1.ce-3.el7
# note: deliberately not starting the daemon here since we really only want
# the CLI client and the expectation that containers made from this image
# will be invoked with '-v /var/run/docker.sock:/var/run/docker.sock'

# install docker-compose
RUN install docker-compose-Linux-x86_64-1.22.0 /usr/bin/docker-compose && \
  rm docker-compose-Linux-x86_64-1.22.0

# Install Maven
RUN mkdir -p /var/opt/maven && \
  tar zxf maven-3.5.4-bin.tar.gz -C /var/opt && \
  mv global-maven-settings.xml /var/opt/apache-maven-3.5.4/conf/settings.xml && \
  rm maven-3.5.4-bin.tar.gz

# install doxygen
RUN tar zxf doxygen-1.8.15.tgz && \
  pushd doxygen-1.8.15 && \
  mkdir build && \
  cd build && \
  cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX=/usr .. && \
  make && \
  make install && \
  popd && \
  rm -rf doxygen-1.8.15 doxygen-1.8.15.tgz

# Install Git
RUN ln -s /usr/bin/db2x_docbook2texi /usr/bin/docbook2x-texi && \
  tar zxf git-2.19.1.tar.gz && \
  pushd git-2.19.1 && \
  make configure && \
  ./configure --prefix=/usr && \
  make all doc info && \
  make install install-doc install-html install-info && \
  popd && \
  rm -rf git-2.19.1.tar.gz git-2.19.1

# Install Git LFS
RUN tar zxf git-lfs-linux-amd64-v2.5.2.tar.gz git-lfs && \
  install git-lfs /usr/bin/git-lfs && \
  rm git-lfs git-lfs-linux-amd64-v2.5.2.tar.gz

# install texlive basic scheme
RUN tar zxf install-tl2018-unx.tar.gz && \
  pushd install-tl-* && \
  ./install-tl --location http://mirror.hmc.edu/ctan/systems/texlive/tlnet \
    --profile ../texlive.profile && \
  popd && \
  rm -rf texlive.profile install-tl*

# install additional packages required by Doxygen
RUN tlmgr install epstopdf float xcolor tabu varwidth fancyvrb multirow \
  hanging adjustbox xkeyval collectbox stackengine etoolbox listofitems ulem \
  wasysym collection-fontsrecommended sectsty tocloft natded newunicodechar \
  caption etoc

ENV MANPATH /usr/local/texlive/2018/texmf-dist/doc/man:$MANPATH
ENV INFOPATH /usr/local/texlive/2018/texmf-dist/doc/info:$INFOPATH
ENV PLANTUML_JAR /var/opt/plantuml/plantuml.jar
ENV GRAPHVIZ_DOT /usr/bin/dot
ENV DOXYGEN_EXE /usr/bin/doxygen
ENV PATH /var/opt/apache-maven-3.5.4/bin:/usr/local/texlive/2018/bin/x86_64-linux:$PATH
