_Copyright &copy; 2018 Kirk Rader_

_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at_

> <http://www.apache.org/licenses/LICENSE-2.0>

_Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._

# Containerized Java Build Environment

Docker configuration and build scripts for creating the `registery.gitlab.com/kirkrader/java-build/base` and related Docker images.

A common issue for software development organizations is how to ensure that individual developers, test automation engineers and CI/CD server build jobs all use exactly the same build environment. Containers offer a solution to this dilemma. However, care must be made to keep the base development environment configuration separate from any user-specific credentials, SSH keys etc. The files in this repository provide an example of one approach: maintain one Dockerfile, _base.dockerfile_, for a base image containing the common build environment configuration (tool chain, library dependencies etc.) and a separate Dockerfile, _user.dockefile_, for use by individual users to create personalized containers with sensitive information that varies for each user.

To clone this repository:

    git clone https://gitlab.com/kirkrader/java-build.git

To pull the pre-built base image using the [deploy token](https://gitlab.com/help/user/project/deploy_tokens/index#read-container-registry-images) created for use by the public:

    # use the following deploy token credentials if you do not already have your own gitlab.com user id;
    # otherwise, preferably, use your gitlab.com credentials with docker login:
    docker login -u gitlab+deploy-token-21516 -p 5hXpzUMGquta3ALHEnZb registry.gitlab.com

    # once logged in, you can pull the pre-built base image:
    docker pull registry.gitlab.com/kirkrader/java-build/base:latest

> **Note:** when making changes to [base.dockerfile](https://gitlab.com/kirkrader/java-build/blob/master/docker/base.dockerfile) or [user.dockerfile](https://gitlab.com/kirkrader/java-build/blob/master/docker/user.dockerfile) it is important to remember to keep all features that should be common to all users and the CI/CD build server in [base.dockerfile](https://gitlab.com/kirkrader/java-build/blob/master/docker/base.dockerfile) while only [user.dockerfile](https://gitlab.com/kirkrader/java-build/blob/master/docker/user.dockerfile) should contain sensitive, user-specific information like user names, SSH keys etc.

See <https://kirkrader.gitlab.io/java-build/quick-start.html> for a brief introduction and guide to getting started.

See <https://kirkrader.gitlab.io/java-build/> for full documentation.

## Summary

Following procedures similar to those described in [java-build](https://kirkrader.gitlab.io/java-build/) (<https://kirkrader.gitlab.io/java-build/>), every user will have access to a containerized build environment identical to one another's and to that used on the CI/CD server.

- DevOps uses [build-base-image](https://gitlab.com/kirkrader/java-build/blob/master/build-base-image) to create a common, shared build environment image

- Users use [build-user-image](https://gitlab.com/kirkrader/java-build/blob/master/build-user-image) to create personal images customized with the appropriate SSH keys, Maven settings etc.

- DevOps can also use [build-user-image](https://gitlab.com/kirkrader/java-build/blob/master/build-user-image) to create images for use on CI/CD servers and the like

- [build-base-image](https://gitlab.com/kirkrader/java-build/blob/master/build-base-image) uses [base.dockerfile](docker/base.dockerfile) to build an image suitable for pushing to a shared repository

- [build-user-image](https://gitlab.com/kirkrader/java-build/blob/master/build-user-image) uses [user.dockerfile](docker/user.dockerfile) to create images customized for a particular user or CI/CD process

- [run-java-build-container](https://gitlab.com/kirkrader/java-build/blob/master/run-java-build-container) provides a convenience wrapper for creating and running containers from the current user's personal image

- [user.dockerfile](https://gitlab.com/kirkrader/java-build/blob/master/docker/user.dockerfile) and [run-java-build-container](https://gitlab.com/kirkrader/java-build/blob/master/run-java-build-container) support mapping file system directories into the user's container

    - `/projects` mount point to hold source file working directories, built artifacts etc.

    - `/mvn_repo` mount point to store the local Maven repository
